
#include <SPI.h>
#include <RH_RF69.h>
#include <Arduino.h>
#include <ArduinoSTL.h>
#include <queue>
#include "Beacon.h"
#include "Field.h"
#include "wiring_private.h"

#define SENSOR_RX 10
#define SENSOR_TX 11

#define SENSOR_EN 12
#define MAGNETS_EN 9
#define PUMP_EN 6
#define CAM1_EN A1
#define CAM2_EN A2
#define CAM3_EN A3
#define CAM4_EN A4

#define RFM69_CS      8
#define RFM69_INT     3
#define RFM69_RST     4

#define NB_REQUESTS 5
#define NB_SWITCHABLE_ELEMENTS 7
#define NB_ORIENTABLE_CAMS 2

enum requests { 
	DISTANCE_TO_WALL, ANGLE_X, ANGLE_Y, 
	LIDAR_DATA_START_STREAM, LIDAR_DATA_STOP_STREAM 
};
enum switchable_elements {PUMP, MAGNETS, SENSORS, CAM1, CAM2, CAM3, CAM4};

RH_RF69 rf69 = RH_RF69(8,3);
Uart Serial2(&sercom1, SENSOR_TX, SENSOR_RX, SERCOM_RX_PAD_0, UART_TX_PAD_2);

std::queue<Field*> responseQueue;
uint8_t response[RH_RF69_MAX_MESSAGE_LEN];

void SERCOM1_Handler()
{
	Serial2.IrqHandler();
}

void setup()
{
	Serial.begin(115200);
	Serial2.begin(115200);

	pinMode(SENSOR_EN, OUTPUT);
	pinMode(MAGNETS_EN, OUTPUT);
	pinMode(PUMP_EN, OUTPUT);
	pinMode(CAM1_EN, OUTPUT);
	pinMode(CAM2_EN, OUTPUT);
	pinMode(CAM3_EN, OUTPUT);
	pinMode(CAM4_EN, OUTPUT);

	pinPeripheral(SENSOR_RX, PIO_SERCOM);
	pinPeripheral(SENSOR_TX, PIO_SERCOM);

	digitalWrite(SENSOR_EN, LOW);
	digitalWrite(MAGNETS_EN, HIGH);
	digitalWrite(PUMP_EN, HIGH);
	digitalWrite(CAM1_EN, HIGH);
	digitalWrite(CAM2_EN, HIGH);
	digitalWrite(CAM3_EN, HIGH);
	digitalWrite(CAM4_EN, HIGH);

	pinMode(RFM69_RST, OUTPUT);
	digitalWrite(RFM69_RST, LOW);

	Serial.println("Feather RFM69 RX Test!");
	Serial.println();

	// manual reset
	digitalWrite(RFM69_RST, HIGH);
	delay(10);
	digitalWrite(RFM69_RST, LOW);
	delay(10);

	if (!rf69.init())
		Serial.println("init failed");
	
	// Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
	// No encryption
	if (!rf69.setFrequency(868.0))
		Serial.println("setFrequency failed");

	// If you are using a high power RF69, you *must* set a Tx power in the
	// range 14 to 20 like this:
	rf69.setTxPower(20);

	// The encryption key has to be the same as the one in the server
	// 16 bytes
	uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 };
	rf69.setEncryptionKey(key);

}

void loop()
{
	if (getBeacon())
	{
		sendAnswer();
	}

}
void sendAnswer()
{
	int byte = 0;
	//send all we got at once
	while (!responseQueue.empty())
	{
		byte = 0;
		//send packets until queue is empty
		while (byte + responseQueue.front()->length + 1 < RH_RF69_MAX_MESSAGE_LEN && !responseQueue.empty())
		{
			//copy header
			response[byte] = responseQueue.front()->header;
			//copy data
			for (int i = 0; i < responseQueue.front()->length; i++)
			{
				response[++byte] = responseQueue.front()->data[i];
			}
			//remove old element
			responseQueue.pop();
		}
		//send max length message
		rf69.send(response, byte);
		rf69.waitPacketSent();
	}
}
bool readBeacon(Beacon b) 
{
	//reads the beacon's content and applies user's commands

	//enable/disable components
	Serial.print("0b");
	for (uint8_t i = 0; i < 8; i++)
	{
		bool en_i = (b.en >> i) & 0x01;
		Serial.print(en_i); 
		switchElement(i, en_i);
	}
	Serial.println();
	Serial.print("EN: "); Serial.println(b.en);
	if (b.panTilt != 0x00)
	{
		//left/right/up/down takes 4 bits for every pan/tilt system
		//2 cams with pan/tilts systems : all takes 8 bits
		uint8_t i = 0;
		while(!(b.panTilt >> i) & 0x01)
		{
			i++;
		}
		uint8_t nbCam = i / 4;
		uint8_t direction = i % 4;
		
		//we must ask the servocontroller feather M0 to do so
		//custom protocol only for test purposes : "CAM,<nbcam>,<direction> \n"
		//<direction> is 0,1,2,3 for left,right,up,down
		//<nbcam> starts at 0
		Serial2.print("CAM,");
		Serial2.print((char)(i+0x30));
		Serial2.print(',');
		switch (direction)
		{
			case 0:	Serial2.println('0'); break;
			case 1:	Serial2.println('1'); break;
			case 2:	Serial2.println('2'); break;
			case 3:	Serial2.println('3'); break;
		}
	}
	//send data to remote
	if (b.req != 0x00)
	{
		for (uint8_t i = 0; i < NB_REQUESTS; i++)
		{
			bool req_i = (b.req >> i) & 0x01;
			if(req_i) getResponse(i);
		}
		return true;
	}
	return false;
}
bool getBeacon()
{
	uint8_t msg[3];
	if (rf69.available())
	{
		// Should be a message for us now   
		uint8_t len = sizeof(msg);
		if (rf69.recv(msg, &len))
		{
			Beacon b(msg);
			return readBeacon(b);
		}
	}
	return false;
}
void switchElement(uint8_t index, bool enable)
{
	switch (index)
	{
		case PUMP:		
			digitalWrite(PUMP_EN, enable);
			break;
		case MAGNETS:	
			digitalWrite(MAGNETS_EN , enable);
			break;
		case SENSORS:	
			digitalWrite(SENSOR_EN, enable);
			break;
		case CAM1:		
			digitalWrite(CAM1_EN, enable);
			break;
		case CAM2:		
			digitalWrite(CAM2_EN, enable);
			break;
		case CAM3:		
			digitalWrite(CAM3_EN, enable);
			break;
		case CAM4:		
			digitalWrite(CAM4_EN, enable);
			break;
	}
}
void getResponse(uint8_t index)
{
	uint8_t* data = new uint8_t(0);
	switch (index)
	{
		case DISTANCE_TO_WALL:	
			*data = 1;
			addToResponse(index, data, 1);
			break;
		case ANGLE_X :
			*data = 2;
			addToResponse(index, data, 1);
			break;
		case ANGLE_Y:
			*data = 3;
			addToResponse(index, data, 1);
			break;
		case LIDAR_DATA_START_STREAM: 
			*data = 4;
			addToResponse(index, data, 1);
			break;
		case LIDAR_DATA_STOP_STREAM:
			*data = 5;
			addToResponse(index, data, 1);
			break;
		default:
			break;
	}
}
void addToResponse(uint8_t h, uint8_t* d, uint8_t l)
{
	Field* f = new Field();
	f->header = h;
	f->data = d;
	f->length = l;
	responseQueue.push(f);
}