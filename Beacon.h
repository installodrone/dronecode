#pragma once
#include <stdint.h>
class Beacon {
public:
	uint8_t en = 0;
	uint8_t panTilt = 0;
	uint8_t req = 0;
	Beacon(uint8_t buffer[])
	{
		en = buffer[0];
		panTilt = buffer[1];
		req = buffer[2];
	}
};

